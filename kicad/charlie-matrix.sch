EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D?
U 1 1 5D5FFDF6
P 900 750
F 0 "D?" H 892 586 50  0001 C CNN
F 1 "LED" H 892 586 50  0001 C CNN
F 2 "" H 900 750 50  0001 C CNN
F 3 "~" H 900 750 50  0001 C CNN
	1    900  750 
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFDFC
P 900 850
F 0 "D?" H 892 686 50  0001 C CNN
F 1 "LED" H 892 686 50  0001 C CNN
F 2 "" H 900 850 50  0001 C CNN
F 3 "~" H 900 850 50  0001 C CNN
	1    900  850 
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFE02
P 900 950
F 0 "D?" H 892 786 50  0001 C CNN
F 1 "LED" H 892 786 50  0001 C CNN
F 2 "" H 900 950 50  0001 C CNN
F 3 "~" H 900 950 50  0001 C CNN
	1    900  950 
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFE08
P 900 1050
F 0 "D?" H 892 886 50  0001 C CNN
F 1 "LED" H 892 886 50  0001 C CNN
F 2 "" H 900 1050 50  0001 C CNN
F 3 "~" H 900 1050 50  0001 C CNN
	1    900  1050
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFE0E
P 900 1150
F 0 "D?" H 892 986 50  0001 C CNN
F 1 "LED" H 892 986 50  0001 C CNN
F 2 "" H 900 1150 50  0001 C CNN
F 3 "~" H 900 1150 50  0001 C CNN
	1    900  1150
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFE14
P 900 1250
F 0 "D?" H 892 1086 50  0001 C CNN
F 1 "LED" H 892 1086 50  0001 C CNN
F 2 "" H 900 1250 50  0001 C CNN
F 3 "~" H 900 1250 50  0001 C CNN
	1    900  1250
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFE1A
P 900 1350
F 0 "D?" H 892 1186 50  0001 C CNN
F 1 "LED" H 892 1186 50  0001 C CNN
F 2 "" H 900 1350 50  0001 C CNN
F 3 "~" H 900 1350 50  0001 C CNN
	1    900  1350
	-1   0    0    1   
$EndComp
Text Label 750  750  0    50   ~ 0
L8
Text Label 1050 750  0    50   ~ 0
L10
Text Label 750  850  0    50   ~ 0
L7
Text Label 1050 850  0    50   ~ 0
L11
Text Label 750  950  0    50   ~ 0
L6
Text Label 750  1050 0    50   ~ 0
L5
Text Label 750  1150 0    50   ~ 0
L4
Text Label 750  1250 0    50   ~ 0
L3
Text Label 750  1350 0    50   ~ 0
L2
Text Label 1050 1350 0    50   ~ 0
L1
Text Label 1050 1250 0    50   ~ 0
L15
Text Label 1050 1150 0    50   ~ 0
L14
Text Label 1050 1050 0    50   ~ 0
L13
Text Label 1050 950  0    50   ~ 0
L12
$Comp
L Device:LED D?
U 1 1 5D5FFED6
P 1400 750
F 0 "D?" H 1392 586 50  0001 C CNN
F 1 "LED" H 1392 586 50  0001 C CNN
F 2 "" H 1400 750 50  0001 C CNN
F 3 "~" H 1400 750 50  0001 C CNN
	1    1400 750 
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFEDC
P 1400 850
F 0 "D?" H 1392 686 50  0001 C CNN
F 1 "LED" H 1392 686 50  0001 C CNN
F 2 "" H 1400 850 50  0001 C CNN
F 3 "~" H 1400 850 50  0001 C CNN
	1    1400 850 
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFEE2
P 1400 950
F 0 "D?" H 1392 786 50  0001 C CNN
F 1 "LED" H 1392 786 50  0001 C CNN
F 2 "" H 1400 950 50  0001 C CNN
F 3 "~" H 1400 950 50  0001 C CNN
	1    1400 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFEE8
P 1400 1050
F 0 "D?" H 1392 886 50  0001 C CNN
F 1 "LED" H 1392 886 50  0001 C CNN
F 2 "" H 1400 1050 50  0001 C CNN
F 3 "~" H 1400 1050 50  0001 C CNN
	1    1400 1050
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFEEE
P 1400 1150
F 0 "D?" H 1392 986 50  0001 C CNN
F 1 "LED" H 1392 986 50  0001 C CNN
F 2 "" H 1400 1150 50  0001 C CNN
F 3 "~" H 1400 1150 50  0001 C CNN
	1    1400 1150
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFEF4
P 1400 1250
F 0 "D?" H 1392 1086 50  0001 C CNN
F 1 "LED" H 1392 1086 50  0001 C CNN
F 2 "" H 1400 1250 50  0001 C CNN
F 3 "~" H 1400 1250 50  0001 C CNN
	1    1400 1250
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D5FFEFA
P 1400 1350
F 0 "D?" H 1392 1186 50  0001 C CNN
F 1 "LED" H 1392 1186 50  0001 C CNN
F 2 "" H 1400 1350 50  0001 C CNN
F 3 "~" H 1400 1350 50  0001 C CNN
	1    1400 1350
	-1   0    0    1   
$EndComp
Text Label 1250 850  0    50   ~ 0
L8
Text Label 1550 750  0    50   ~ 0
L10
Text Label 1250 950  0    50   ~ 0
L7
Text Label 1550 850  0    50   ~ 0
L11
Text Label 1250 1050 0    50   ~ 0
L6
Text Label 1250 1150 0    50   ~ 0
L5
Text Label 1250 1250 0    50   ~ 0
L4
Text Label 1250 1350 0    50   ~ 0
L3
Text Label 1550 1350 0    50   ~ 0
L1
Text Label 1550 1250 0    50   ~ 0
L15
Text Label 1550 1150 0    50   ~ 0
L14
Text Label 1550 1050 0    50   ~ 0
L13
Text Label 1550 950  0    50   ~ 0
L12
Text Label 1250 750  0    50   ~ 0
L9
Text Notes 550  750  0    50   ~ 0
Y0
Text Notes 550  850  0    50   ~ 0
Y1
Text Notes 550  950  0    50   ~ 0
Y2
Text Notes 550  1050 0    50   ~ 0
Y3
Text Notes 550  1150 0    50   ~ 0
Y4
Text Notes 550  1250 0    50   ~ 0
Y5
Text Notes 550  1350 0    50   ~ 0
Y6
Text Notes 850  1550 0    50   ~ 0
X0
Text Notes 1350 1550 0    50   ~ 0
X1
$EndSCHEMATC
