EESchema Schematic File Version 4
LIBS:aammf-badge-2019-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L aammf-badge-2019-rescue:Logo_Open_Hardware_Small-Graphic-aammf-badge-2019-rescue #LOGO1
U 1 1 5D60627B
P 7250 6900
F 0 "#LOGO1" H 7250 7175 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 7250 6675 50  0001 C CNN
F 2 "" H 7250 6900 50  0001 C CNN
F 3 "~" H 7250 6900 50  0001 C CNN
	1    7250 6900
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-BUTTON_4P-5.2X5.2X1.5MM-SKQGADE010_-OPL_Switch-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue SW1
U 1 1 5CE9036F
P 4850 4550
F 0 "SW1" H 4850 4904 45  0000 C CNN
F 1 "Button" H 4800 4000 45  0000 C CNN
F 2 "misc:SMD-5.2X5.2X1.5MM-tactile-switch" H 4850 4550 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/1812131830_ALPS-Electric-SKQGADE010_C116647.pdf" H 4850 4550 50  0001 C CNN
F 4 "SKQGADE010" H 4880 4700 20  0001 C CNN "MPN"
F 5 "311020017" H 4880 4700 20  0001 C CNN "seeed-SKU"
F 6 "ALPS Electric" H 4850 4550 50  0001 C CNN "Manufacturer"
F 7 "C116647" H 4850 4550 50  0001 C CNN "lcsc-part"
	1    4850 4550
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:LED-SMD-RED-DIFFUSED_0603_-OPL_Optoelectronics-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue D1
U 1 1 5CE92CC2
P 2450 4550
F 0 "D1" V 2350 4650 45  0000 C CNN
F 1 "RED" V 2550 4650 45  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 2450 4550 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Everlight-Elec-19-217-R6C-AL1M2VY-3T-GDB_C131266.pdf" H 2450 4550 50  0001 C CNN
F 4 "19-217/R6C-AL1M2VY/3T(GDB) " H 2480 4700 20  0001 C CNN "MPN"
F 5 "304090042" H 2480 4700 20  0001 C CNN "seeed-SKU"
F 6 "Everlight Elec" V 2450 4550 50  0001 C CNN "Manufacturer"
F 7 "C131266" V 2450 4550 50  0001 C CNN "lcsc-part"
	1    2450 4550
	0    1    1    0   
$EndComp
$Comp
L aammf-badge-2019-rescue:PMIC-LDO-XC6206P332MR-G_SOT23_-OPL_Integrated_Circuit-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue U1
U 1 1 5CE9469D
P 8850 3550
F 0 "U1" H 8850 3750 45  0000 C CNN
F 1 "LDO 3.3V" H 8850 3600 45  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8850 3550 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/1811201212_hengjiaxing-HX6206P332MR_C296230.pdf" H 8850 3550 50  0001 C CNN
F 4 "XC6201P332MR-G or HX6206P332MR" H 8880 3700 20  0001 C CNN "MPN"
F 5 "310030099" H 8880 3700 20  0001 C CNN "seeed-SKU"
F 6 "Torex Semicon or hengjiaxing" H 8850 3550 50  0001 C CNN "Manufacturer"
F 7 "C29404 or C296230" H 8850 3550 50  0001 C CNN "lcsc-part"
F 8 "https://datasheet.lcsc.com/szlcsc/Torex-Semicon-XC6201P332MR_C29404.pdf" H 8850 3550 50  0001 C CNN "DatasheetAlt"
	1    8850 3550
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-CRYSTAL-ABS07-32.768KHZ-7PF-20PPM-70K_2P-3.2X1.5MM_-OPL_Crystal_Oscillator-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue Y1
U 1 1 5CE95939
P 2400 1350
F 0 "Y1" H 2400 1574 45  0000 C CNN
F 1 "32.768KHZ" V 2650 1450 45  0000 C CNN
F 2 "Crystal:Crystal_SMD_MicroCrystal_CC7V-T1A-2Pin_3.2x1.5mm" H 2400 1350 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/1901081604_Seiko-Epson-Q13FC1350000400_C32346.pdf" H 2400 1350 50  0001 C CNN
F 4 "Q13FC1350000400" H 2430 1500 20  0001 C CNN "MPN"
F 5 "306010055" H 2430 1500 20  0001 C CNN "seeed-SKU"
F 6 "Seiko Epson" H 2400 1350 50  0001 C CNN "Manufacturer"
F 7 "C32346" H 2400 1350 50  0001 C CNN "lcsc-part"
	1    2400 1350
	0    1    1    0   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_ARM_JTAG_SWD_10-Connector-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue J3
U 1 1 5CE996AF
P 1850 6750
F 0 "J3" H 1407 6796 50  0000 R CNN
F 1 "Conn_ARM_JTAG_SWD_10" H 2900 6150 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x05_P1.27mm_Vertical_SMD" H 1900 6200 50  0001 L TNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf" V 1500 5500 50  0001 C CNN
F 4 "np" H 1850 6750 50  0001 C CNN "NP"
	1    1850 6750
	1    0    0    -1  
$EndComp
$Comp
L samd21g18a:SAMD21G18A-samd21g18-samd21-dev-rescue IC1
U 1 1 5CE9ADC1
P 5000 2600
F 0 "IC1" H 5050 4569 100 0000 C CNN
F 1 "SAMD21G18A-M*" H 5050 4403 100 0000 C CNN
F 2 "Package_DFN_QFN:QFN-48-1EP_7x7mm_P0.5mm_EP5.3x5.3mm" H 5050 4275 50  0000 C CNN
F 3 "http://ww1.microchip.com/downloads/en/devicedoc/40001884a.pdf" H 5000 2600 50  0001 C CNN
F 4 "SAMD21G18A-M*" H 5000 2600 50  0001 C CNN "MPN"
F 5 "310010095" H 5000 2600 50  0001 C CNN "seeed-SKU"
F 6 "Atmel" H 5000 2600 50  0001 C CNN "Manufacturer"
	1    5000 2600
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0101
U 1 1 5CE9F792
P 1850 6000
F 0 "#PWR0101" H 1850 5850 50  0001 C CNN
F 1 "+3.3V" H 1865 6173 50  0000 C CNN
F 2 "" H 1850 6000 50  0001 C CNN
F 3 "" H 1850 6000 50  0001 C CNN
	1    1850 6000
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0102
U 1 1 5CEA02F8
P 1850 7500
F 0 "#PWR0102" H 1850 7250 50  0001 C CNN
F 1 "GND" H 1855 7327 50  0000 C CNN
F 2 "" H 1850 7500 50  0001 C CNN
F 3 "" H 1850 7500 50  0001 C CNN
	1    1850 7500
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:CERAMIC-100NF-50V-10%-X7R_0603_-OPL_Capacitor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue C8
U 1 1 5CEA11BE
P 7250 1800
F 0 "C8" H 7250 1700 45  0000 C CNN
F 1 "100 nF" H 7200 1900 45  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7250 1800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Samsung-Electro-Mechanics-CL10B104KA8NNNC_C1590.pdf" H 7250 1800 50  0001 C CNN
F 4 "CL10B104KA8NNNC" H 7280 1950 20  0001 C CNN "MPN"
F 5 "302010138" H 7280 1950 20  0001 C CNN "seeed-SKU"
F 6 "C1590" H 7250 1800 50  0001 C CNN "lcsc-part"
F 7 "Samsung Electro-Mechanics" H 7250 1800 50  0001 C CNN "Manufacturer"
	1    7250 1800
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:CERAMIC-100NF-50V-10%-X7R_0603_-OPL_Capacitor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue C4
U 1 1 5CEA26AB
P 2100 1200
F 0 "C4" H 2000 1250 45  0000 C CNN
F 1 "10 pF" H 2100 1100 45  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2100 1200 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-CC0603JRNPO9BN100_C106245.pdf" H 2100 1200 50  0001 C CNN
F 4 "CC0603JRNPO9BN100" H 2130 1350 20  0001 C CNN "MPN"
F 5 "302010097" H 2130 1350 20  0001 C CNN "seeed-SKU"
F 6 "YAGEO" H 2100 1200 50  0001 C CNN "Manufacturer"
F 7 "C106245" H 2100 1200 50  0001 C CNN "lcsc-part"
	1    2100 1200
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:CERAMIC-100NF-50V-10%-X7R_0603_-OPL_Capacitor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue C6
U 1 1 5CEA38F5
P 8200 3800
F 0 "C6" V 8158 3868 45  0000 L CNN
F 1 "1uF" V 8250 3850 45  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8200 3800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Samsung-Electro-Mechanics-CL10B105KP8NNNC_C95843.pdf" H 8200 3800 50  0001 C CNN
F 4 "CL10B105KP8NNNC" H 8230 3950 20  0001 C CNN "MPN"
F 5 "302010139" H 8230 3950 20  0001 C CNN "seeed-SKU"
F 6 "Samsung Electro-Mechanics" V 8200 3800 50  0001 C CNN "Manufacturer"
F 7 "C95843" V 8200 3800 50  0001 C CNN "lcsc-part"
	1    8200 3800
	0    1    1    0   
$EndComp
$Comp
L aammf-badge-2019-rescue:CERAMIC-100NF-50V-10%-X7R_0603_-OPL_Capacitor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue C7
U 1 1 5CEA4BE4
P 10650 4350
F 0 "C7" V 10608 4418 45  0000 L CNN
F 1 "1uF" V 10700 4400 45  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10650 4350 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Samsung-Electro-Mechanics-CL10B105KP8NNNC_C95843.pdf" H 10650 4350 50  0001 C CNN
F 4 "CL10B105KP8NNNC" H 10680 4500 20  0001 C CNN "MPN"
F 5 "302010139" H 10680 4500 20  0001 C CNN "seeed-SKU"
F 6 "Samsung Electro-Mechanics" V 10650 4350 50  0001 C CNN "Manufacturer"
F 7 "C95843" V 10650 4350 50  0001 C CNN "lcsc-part"
	1    10650 4350
	0    1    1    0   
$EndComp
$Comp
L aammf-badge-2019-rescue:CERAMIC-100NF-50V-10%-X7R_0603_-OPL_Capacitor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue C5
U 1 1 5CEA5786
P 2100 1500
F 0 "C5" H 2000 1550 45  0000 C CNN
F 1 "10 pF" H 2100 1400 45  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2100 1500 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-CC0603JRNPO9BN100_C106245.pdf" H 2100 1500 50  0001 C CNN
F 4 "CC0603JRNPO9BN100" H 2130 1650 20  0001 C CNN "MPN"
F 5 "302010097" H 2130 1650 20  0001 C CNN "seeed-SKU"
F 6 "YAGEO" H 2100 1500 50  0001 C CNN "Manufacturer"
F 7 "C106245" H 2100 1500 50  0001 C CNN "lcsc-part"
	1    2100 1500
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-RES-1.5K-5%-1_10W_0603_-OPL_Resistor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue R1
U 1 1 5CEA618B
P 2450 4900
F 0 "R1" V 2408 4948 45  0000 L CNN
F 1 "470 Ohm" V 2492 4948 45  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2450 4900 40  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-RC0603FR-07470RL_C114669.pdf" H 2450 4900 40  0001 C CNN
F 4 "RC0603JR-071K5L" H 2480 5050 20  0001 C CNN "MPN"
F 5 "301010114" H 2480 5050 20  0001 C CNN "seeed-SKU"
F 6 "YAGEO" V 2450 4900 50  0001 C CNN "Manufacturer"
F 7 "C114669" V 2450 4900 50  0001 C CNN "lcsc-part"
	1    2450 4900
	0    1    1    0   
$EndComp
Text GLabel 3750 6600 2    50   Input ~ 0
D_MINUS
Text GLabel 3750 6700 2    50   Input ~ 0
D_PLUS
Text GLabel 2350 6450 2    50   Input ~ 0
RESET
Text GLabel 2350 6650 2    50   Input ~ 0
SWDCLK
Text GLabel 2350 6750 2    50   Input ~ 0
SWDIO
Wire Wire Line
	1850 6000 1850 6150
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0104
U 1 1 5CEB6BEC
P 4400 4850
F 0 "#PWR0104" H 4400 4600 50  0001 C CNN
F 1 "GND" H 4405 4677 50  0000 C CNN
F 2 "" H 4400 4850 50  0001 C CNN
F 3 "" H 4400 4850 50  0001 C CNN
	1    4400 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4450 4500 4450
Wire Wire Line
	4500 4650 4400 4650
Wire Wire Line
	4400 4650 4400 4800
Text GLabel 4350 4300 0    50   Input ~ 0
RESET
Text GLabel 8000 3550 0    50   Input ~ 0
V_USB
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0106
U 1 1 5CEC203B
P 8200 4100
F 0 "#PWR0106" H 8200 3850 50  0001 C CNN
F 1 "GND" H 8205 3927 50  0000 C CNN
F 2 "" H 8200 4100 50  0001 C CNN
F 3 "" H 8200 4100 50  0001 C CNN
	1    8200 4100
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0107
U 1 1 5CEC243D
P 8850 4100
F 0 "#PWR0107" H 8850 3850 50  0001 C CNN
F 1 "GND" H 8855 3927 50  0000 C CNN
F 2 "" H 8850 4100 50  0001 C CNN
F 3 "" H 8850 4100 50  0001 C CNN
	1    8850 4100
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0108
U 1 1 5CEC2861
P 10650 4550
F 0 "#PWR0108" H 10650 4300 50  0001 C CNN
F 1 "GND" H 10655 4377 50  0000 C CNN
F 2 "" H 10650 4550 50  0001 C CNN
F 3 "" H 10650 4550 50  0001 C CNN
	1    10650 4550
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0109
U 1 1 5CEC2C1F
P 10750 4100
F 0 "#PWR0109" H 10750 3950 50  0001 C CNN
F 1 "+3.3V" H 10765 4273 50  0000 C CNN
F 2 "" H 10750 4100 50  0001 C CNN
F 3 "" H 10750 4100 50  0001 C CNN
	1    10750 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 3550 8200 3550
Wire Wire Line
	8200 3650 8200 3550
Connection ~ 8200 3550
Wire Wire Line
	8200 3550 8350 3550
Wire Wire Line
	8200 3950 8200 4100
Wire Wire Line
	8850 3850 8850 4100
Wire Wire Line
	10650 4500 10650 4550
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0110
U 1 1 5CEC8216
P 2450 5100
F 0 "#PWR0110" H 2450 4850 50  0001 C CNN
F 1 "GND" H 2455 4927 50  0000 C CNN
F 2 "" H 2450 5100 50  0001 C CNN
F 3 "" H 2450 5100 50  0001 C CNN
	1    2450 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 4350 2450 4400
Wire Wire Line
	2450 4700 2450 4750
Wire Wire Line
	2450 5050 2450 5100
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0111
U 1 1 5CECF927
P 1800 1350
F 0 "#PWR0111" H 1800 1100 50  0001 C CNN
F 1 "GND" V 1805 1222 50  0000 R CNN
F 2 "" H 1800 1350 50  0001 C CNN
F 3 "" H 1800 1350 50  0001 C CNN
	1    1800 1350
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 1300 2700 1200
Wire Wire Line
	2700 1200 2400 1200
Connection ~ 2400 1200
Wire Wire Line
	2400 1200 2250 1200
Wire Wire Line
	2700 1400 2700 1500
Wire Wire Line
	2700 1500 2400 1500
Connection ~ 2400 1500
Wire Wire Line
	2400 1500 2250 1500
Wire Wire Line
	1950 1500 1800 1500
Wire Wire Line
	1800 1500 1800 1350
Wire Wire Line
	1950 1200 1800 1200
Wire Wire Line
	1800 1200 1800 1350
Connection ~ 1800 1350
Wire Wire Line
	2700 1300 3300 1300
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0112
U 1 1 5CED6E34
P 6800 1900
F 0 "#PWR0112" H 6800 1650 50  0001 C CNN
F 1 "GND" V 6805 1772 50  0000 R CNN
F 2 "" H 6800 1900 50  0001 C CNN
F 3 "" H 6800 1900 50  0001 C CNN
	1    6800 1900
	0    -1   -1   0   
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0113
U 1 1 5CED745E
P 3300 3200
F 0 "#PWR0113" H 3300 2950 50  0001 C CNN
F 1 "GND" V 3305 3072 50  0000 R CNN
F 2 "" H 3300 3200 50  0001 C CNN
F 3 "" H 3300 3200 50  0001 C CNN
	1    3300 3200
	0    1    1    0   
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0114
U 1 1 5CED830B
P 3300 1700
F 0 "#PWR0114" H 3300 1450 50  0001 C CNN
F 1 "GND" V 3305 1572 50  0000 R CNN
F 2 "" H 3300 1700 50  0001 C CNN
F 3 "" H 3300 1700 50  0001 C CNN
	1    3300 1700
	0    1    1    0   
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0115
U 1 1 5CEDA387
P 7500 1800
F 0 "#PWR0115" H 7500 1550 50  0001 C CNN
F 1 "GND" V 7505 1672 50  0000 R CNN
F 2 "" H 7500 1800 50  0001 C CNN
F 3 "" H 7500 1800 50  0001 C CNN
	1    7500 1800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7500 1800 7400 1800
Text GLabel 6800 2100 2    50   Input ~ 0
RESET
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0116
U 1 1 5CEDDD02
P 6800 1700
F 0 "#PWR0116" H 6800 1550 50  0001 C CNN
F 1 "+3.3V" V 6800 1800 50  0000 L CNN
F 2 "" H 6800 1700 50  0001 C CNN
F 3 "" H 6800 1700 50  0001 C CNN
	1    6800 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 1800 7100 1800
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0117
U 1 1 5CEE207F
P 6800 2700
F 0 "#PWR0117" H 6800 2550 50  0001 C CNN
F 1 "+3.3V" V 6815 2828 50  0000 L CNN
F 2 "" H 6800 2700 50  0001 C CNN
F 3 "" H 6800 2700 50  0001 C CNN
	1    6800 2700
	0    1    1    0   
$EndComp
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0118
U 1 1 5CEE2909
P 3300 3100
F 0 "#PWR0118" H 3300 2950 50  0001 C CNN
F 1 "+3.3V" V 3315 3228 50  0000 L CNN
F 2 "" H 3300 3100 50  0001 C CNN
F 3 "" H 3300 3100 50  0001 C CNN
	1    3300 3100
	0    -1   -1   0   
$EndComp
Text GLabel 6800 3000 2    50   Input ~ 0
D_MINUS
Text GLabel 6800 2900 2    50   Input ~ 0
D_PLUS
Text GLabel 6800 1600 2    50   Input ~ 0
SWDCLK
Text GLabel 6800 1500 2    50   Input ~ 0
SWDIO
$Comp
L aammf-badge-2019-rescue:CERAMIC-100NF-50V-10%-X7R_0603_-OPL_Capacitor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue C3
U 1 1 5CEE406A
P 3100 4700
F 0 "C3" V 3142 4632 45  0000 R CNN
F 1 "100 nF" H 3050 4650 45  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3100 4700 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Samsung-Electro-Mechanics-CL10B104KA8NNNC_C1590.pdf" H 3100 4700 50  0001 C CNN
F 4 "CL10B104KA8NNNC" H 3130 4850 20  0001 C CNN "MPN"
F 5 "302010138" H 3130 4850 20  0001 C CNN "seeed-SKU"
F 6 "Samsung Electro-Mechanics" V 3100 4700 50  0001 C CNN "Manufacturer"
F 7 "C1590" V 3100 4700 50  0001 C CNN "lcsc-part"
	1    3100 4700
	0    -1   -1   0   
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0119
U 1 1 5CEE6556
P 3100 5000
F 0 "#PWR0119" H 3100 4750 50  0001 C CNN
F 1 "GND" H 3105 4827 50  0000 C CNN
F 2 "" H 3100 5000 50  0001 C CNN
F 3 "" H 3100 5000 50  0001 C CNN
	1    3100 5000
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0120
U 1 1 5CEE6CDD
P 3100 4400
F 0 "#PWR0120" H 3100 4250 50  0001 C CNN
F 1 "+3.3V" H 3115 4573 50  0000 C CNN
F 2 "" H 3100 4400 50  0001 C CNN
F 3 "" H 3100 4400 50  0001 C CNN
	1    3100 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4400 3100 4550
Wire Wire Line
	3100 4850 3100 5000
$Comp
L aammf-badge-2019-rescue:CERAMIC-100NF-50V-10%-X7R_0603_-OPL_Capacitor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue C2
U 1 1 5CEE8870
P 9200 2100
F 0 "C2" V 9242 2032 45  0000 R CNN
F 1 "100 nF" V 9100 2050 45  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9200 2100 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Samsung-Electro-Mechanics-CL10B104KA8NNNC_C1590.pdf" H 9200 2100 50  0001 C CNN
F 4 "CL10B104KA8NNNC" H 9230 2250 20  0001 C CNN "MPN"
F 5 "302010138" H 9230 2250 20  0001 C CNN "seeed-SKU"
F 6 "Samsung Electro-Mechanics" V 9200 2100 50  0001 C CNN "Manufacturer"
F 7 "C1590" V 9200 2100 50  0001 C CNN "lcsc-part"
	1    9200 2100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1850 7350 1850 7450
Wire Wire Line
	1950 7350 1950 7450
Wire Wire Line
	1950 7450 1850 7450
Connection ~ 1850 7450
Wire Wire Line
	1850 7450 1850 7500
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0123
U 1 1 5CEF8F64
P 3300 1800
F 0 "#PWR0123" H 3300 1650 50  0001 C CNN
F 1 "+3.3V" V 3315 1928 50  0000 L CNN
F 2 "" H 3300 1800 50  0001 C CNN
F 3 "" H 3300 1800 50  0001 C CNN
	1    3300 1800
	0    -1   -1   0   
$EndComp
$Comp
L aammf-badge-2019-rescue:CERAMIC-100NF-50V-10%-X7R_0603_-OPL_Capacitor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue C1
U 1 1 5CEF970A
P 2850 4700
F 0 "C1" V 2892 4632 45  0000 R CNN
F 1 "100 nF" H 2800 4650 45  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2850 4700 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Samsung-Electro-Mechanics-CL10B104KA8NNNC_C1590.pdf" H 2850 4700 50  0001 C CNN
F 4 "CL10B104KA8NNNC" H 2880 4850 20  0001 C CNN "MPN"
F 5 "302010138" H 2880 4850 20  0001 C CNN "seeed-SKU"
F 6 "Samsung Electro-Mechanics" V 2850 4700 50  0001 C CNN "Manufacturer"
F 7 "C1590" V 2850 4700 50  0001 C CNN "lcsc-part"
	1    2850 4700
	0    -1   -1   0   
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0124
U 1 1 5CEFA016
P 2850 5000
F 0 "#PWR0124" H 2850 4750 50  0001 C CNN
F 1 "GND" H 2855 4827 50  0000 C CNN
F 2 "" H 2850 5000 50  0001 C CNN
F 3 "" H 2850 5000 50  0001 C CNN
	1    2850 5000
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0125
U 1 1 5CEFA26F
P 2850 4400
F 0 "#PWR0125" H 2850 4250 50  0001 C CNN
F 1 "+3.3V" H 2865 4573 50  0000 C CNN
F 2 "" H 2850 4400 50  0001 C CNN
F 3 "" H 2850 4400 50  0001 C CNN
	1    2850 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 4550 2850 4400
Wire Wire Line
	2850 4850 2850 5000
Text GLabel 3300 1500 0    50   Input ~ 0
PA2
Text GLabel 6800 1400 2    50   Input ~ 0
PB2
Text GLabel 6800 1300 2    50   Input ~ 0
PB3
Text GLabel 3300 2100 0    50   Input ~ 0
PA4
Text GLabel 3300 2200 0    50   Input ~ 0
PA5
Text GLabel 3300 2300 0    50   Input ~ 0
PA6
Text GLabel 6800 3200 2    50   Input ~ 0
PA22
Text GLabel 6800 3100 2    50   Input ~ 0
PA23
Text GLabel 3300 2900 0    50   Input ~ 0
PA10
Text GLabel 3300 3000 0    50   Input ~ 0
PA11
Text GLabel 3300 3300 0    50   Input ~ 0
PB10
Text GLabel 3300 3400 0    50   Input ~ 0
PB11
Text GLabel 6800 3400 2    50   Input ~ 0
PA20
Text GLabel 6800 3300 2    50   Input ~ 0
PA21
Text GLabel 6800 3800 2    50   Input ~ 0
PA16
Text GLabel 6800 3700 2    50   Input ~ 0
PA17
Text GLabel 6800 3500 2    50   Input ~ 0
PA19
Text GLabel 3300 2700 0    50   Input ~ 0
PA8
Text GLabel 3300 2800 0    50   Input ~ 0
PA9
Text GLabel 6800 2300 2    50   Input ~ 0
PB23
Wire Wire Line
	2700 1400 3300 1400
Text GLabel 3300 1600 0    50   Input ~ 0
AREF
Text GLabel 3750 6300 2    50   Input ~ 0
V_USB
NoConn ~ 2350 6850
NoConn ~ 2350 6950
NoConn ~ 1750 7350
Wire Wire Line
	5200 4450 5300 4450
Wire Wire Line
	5300 4450 5300 4300
Wire Wire Line
	5300 4300 4400 4300
Wire Wire Line
	4400 4300 4400 4450
Wire Wire Line
	5200 4650 5300 4650
Wire Wire Line
	5300 4650 5300 4800
Wire Wire Line
	5300 4800 4400 4800
Connection ~ 4400 4800
Wire Wire Line
	4400 4800 4400 4850
Text GLabel 3300 2400 0    50   Input ~ 0
PA7
Text GLabel 6800 2400 2    50   Input ~ 0
PB22
Text GLabel 6800 2200 2    50   Input ~ 0
PA27
Text GLabel 3300 3500 0    50   Input ~ 0
MOSI
Text GLabel 3300 3600 0    50   Input ~ 0
SCK
Text GLabel 3300 3700 0    50   Input ~ 0
CS
Text GLabel 3300 3800 0    50   Input ~ 0
MISO
Text GLabel 3300 1900 0    50   Input ~ 0
PB8
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0128
U 1 1 5CFC3861
P 6800 2800
F 0 "#PWR0128" H 6800 2550 50  0001 C CNN
F 1 "GND" V 6805 2672 50  0000 R CNN
F 2 "" H 6800 2800 50  0001 C CNN
F 3 "" H 6800 2800 50  0001 C CNN
	1    6800 2800
	0    -1   -1   0   
$EndComp
Text GLabel 6800 3600 2    50   Input ~ 0
PA18
$Comp
L aammf-badge-2019-rescue:Badgelife_sao_connector_v169bis-badgelife_shitty_addon_v169bis-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue X1
U 1 1 5CEC6285
P 1050 5000
F 0 "X1" H 1378 5046 50  0000 L CNN
F 1 "Badgelife_sao_connector_v169bis" V 700 4350 50  0000 L CNN
F 2 "badgelife_sao_v169bis:Badgelife-SAOv169-BADGE-2x3" H 1050 5200 50  0001 C CNN
F 3 "https://www.tindie.com/products/twinkletwinkie/10-pack-of-saov169bis-female-keyed-connectors/" H 1050 5200 50  0001 C CNN
F 4 "MTF185-203SY1" H 1050 5000 50  0001 C CNN "MPN"
F 5 "MINTRON" H 1050 5000 50  0001 C CNN "Manufacturer"
F 6 "C358725" H 1050 5000 50  0001 C CNN "lcsc-part"
	1    1050 5000
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0129
U 1 1 5CEC8782
P 850 4450
F 0 "#PWR0129" H 850 4300 50  0001 C CNN
F 1 "+3.3V" H 865 4623 50  0000 C CNN
F 2 "" H 850 4450 50  0001 C CNN
F 3 "" H 850 4450 50  0001 C CNN
	1    850  4450
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0130
U 1 1 5CEC8CD2
P 850 5550
F 0 "#PWR0130" H 850 5300 50  0001 C CNN
F 1 "GND" H 855 5377 50  0000 C CNN
F 2 "" H 850 5550 50  0001 C CNN
F 3 "" H 850 5550 50  0001 C CNN
	1    850  5550
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-BUTTON_4P-5.2X5.2X1.5MM-SKQGADE010_-OPL_Switch-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue SW3
U 1 1 5D608EE2
P 6200 4550
F 0 "SW3" H 6200 4904 45  0000 C CNN
F 1 "Button" H 6200 4000 45  0000 C CNN
F 2 "misc:SMD-5.2X5.2X1.5MM-tactile-switch" H 6200 4550 50  0001 C CNN
F 3 "" H 6200 4550 50  0001 C CNN
	1    6200 4550
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0131
U 1 1 5D608EF2
P 5750 4850
F 0 "#PWR0131" H 5750 4600 50  0001 C CNN
F 1 "GND" H 5755 4677 50  0000 C CNN
F 2 "" H 5750 4850 50  0001 C CNN
F 3 "" H 5750 4850 50  0001 C CNN
	1    5750 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4450 5850 4450
Wire Wire Line
	5850 4650 5750 4650
Wire Wire Line
	5750 4650 5750 4800
Wire Wire Line
	6550 4450 6650 4450
Wire Wire Line
	6650 4450 6650 4300
Wire Wire Line
	6650 4300 5750 4300
Wire Wire Line
	5750 4300 5750 4450
Wire Wire Line
	6550 4650 6650 4650
Wire Wire Line
	6650 4650 6650 4800
Wire Wire Line
	6650 4800 5750 4800
Connection ~ 5750 4800
Wire Wire Line
	5750 4800 5750 4850
$Comp
L aammf-badge-2019-rescue:SMD-BUTTON_4P-5.2X5.2X1.5MM-SKQGADE010_-OPL_Switch-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue SW2
U 1 1 5D60A245
P 4850 5550
F 0 "SW2" H 4850 5904 45  0000 C CNN
F 1 "Button" H 4650 5000 45  0000 C CNN
F 2 "misc:SMD-5.2X5.2X1.5MM-tactile-switch" H 4850 5550 50  0001 C CNN
F 3 "" H 4850 5550 50  0001 C CNN
	1    4850 5550
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0133
U 1 1 5D60A255
P 4400 5850
F 0 "#PWR0133" H 4400 5600 50  0001 C CNN
F 1 "GND" H 4405 5677 50  0000 C CNN
F 2 "" H 4400 5850 50  0001 C CNN
F 3 "" H 4400 5850 50  0001 C CNN
	1    4400 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5450 4500 5450
Wire Wire Line
	4500 5650 4400 5650
Wire Wire Line
	4400 5650 4400 5800
Wire Wire Line
	5200 5450 5300 5450
Wire Wire Line
	5300 5450 5300 5300
Wire Wire Line
	5300 5300 4400 5300
Wire Wire Line
	4400 5300 4400 5450
Wire Wire Line
	5200 5650 5300 5650
Wire Wire Line
	5300 5650 5300 5800
Wire Wire Line
	5300 5800 4400 5800
Connection ~ 4400 5800
Wire Wire Line
	4400 5800 4400 5850
Text GLabel 950  6300 2    50   Input ~ 0
RESET
Text GLabel 950  6400 2    50   Input ~ 0
SWDCLK
Text GLabel 950  6500 2    50   Input ~ 0
SWDIO
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0135
U 1 1 5D612F14
P 950 6200
F 0 "#PWR0135" H 950 6050 50  0001 C CNN
F 1 "+3.3V" H 965 6373 50  0000 C CNN
F 2 "" H 950 6200 50  0001 C CNN
F 3 "" H 950 6200 50  0001 C CNN
	1    950  6200
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0136
U 1 1 5D612FAF
P 950 6600
F 0 "#PWR0136" H 950 6350 50  0001 C CNN
F 1 "GND" H 955 6427 50  0000 C CNN
F 2 "" H 950 6600 50  0001 C CNN
F 3 "" H 950 6600 50  0001 C CNN
	1    950  6600
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x05_Female-Connector-aammf-badge-2019-rescue J4
U 1 1 5D61305F
P 600 6400
F 0 "J4" H 494 5975 50  0000 C CNN
F 1 "swd" H 600 6800 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 600 6400 50  0001 C CNN
F 3 "~" H 600 6400 50  0001 C CNN
F 4 "np" H 600 6400 50  0001 C CNN "NP"
	1    600  6400
	-1   0    0    1   
$EndComp
Wire Wire Line
	950  6200 800  6200
Wire Wire Line
	800  6300 950  6300
Wire Wire Line
	800  6400 950  6400
Wire Wire Line
	800  6500 950  6500
Wire Wire Line
	800  6600 950  6600
$Comp
L aammf-badge-2019-rescue:Battery_Cell-Powersources-battery_cell-powersources-aammf-badge-2019-rescue BT1
U 1 1 5D614CED
P 8200 5600
F 0 "BT1" H 7950 5700 50  0000 L CNN
F 1 "AA" H 8000 5600 50  0000 L CNN
F 2 "flummsy:Double_AA_Battery_Holder_4_Pin" V 8200 5660 50  0001 C CNN
F 3 "https://www.aliexpress.com/item/32723004560.html?spm=a2g0s.9042311.0.0.27424c4dJfjfCB" V 8200 5660 50  0001 C CNN
F 4 "np" H 8200 5600 50  0001 C CNN "NP"
	1    8200 5600
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Battery_Cell-Powersources-battery_cell-powersources-aammf-badge-2019-rescue BT1
U 2 1 5D614DA1
P 8200 6050
F 0 "BT1" H 7950 6200 50  0000 L CNN
F 1 "AA" H 8050 6000 50  0000 L CNN
F 2 "flummsy:Double_AA_Battery_Holder_4_Pin" V 8200 6110 50  0001 C CNN
F 3 "https://www.aliexpress.com/item/32723004560.html?spm=a2g0s.9042311.0.0.27424c4dJfjfCB" V 8200 6110 50  0001 C CNN
F 4 "np" H 8200 6050 50  0001 C CNN "NP"
	2    8200 6050
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0126
U 1 1 5D615097
P 8200 6250
F 0 "#PWR0126" H 8200 6000 50  0001 C CNN
F 1 "GND" H 8205 6077 50  0000 C CNN
F 2 "" H 8200 6250 50  0001 C CNN
F 3 "" H 8200 6250 50  0001 C CNN
	1    8200 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 6250 8200 6150
Wire Wire Line
	8200 5850 8200 5700
Wire Wire Line
	10650 4100 10750 4100
Wire Wire Line
	10650 4100 10650 4200
Wire Wire Line
	8200 5300 8200 5400
$Comp
L aammf-badge-2019-rescue:MICRO-SD-CARD-SOCKET-9P_ST-TF-003A_-OPL_Connector-aammf-badge-2019-rescue SD1
U 1 1 5D631DBE
P 10050 1500
F 0 "SD1" H 10577 1592 45  0000 L CNN
F 1 "Micro SD" H 9800 1500 45  0000 L CNN
F 2 "misc:MICRO-SD9+4P-SMD-16.1X14.5X1.85MM" H 10050 1500 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/SOFNG-TF-015_C113206.pdf" H 10050 1500 50  0001 C CNN
F 4 "TF-015" H 10080 1650 20  0001 C CNN "MPN"
F 5 "320090000" H 10080 1650 20  0001 C CNN "seeed-SKU"
F 6 "np" H 10050 1500 50  0001 C CNN "NP"
F 7 "SOFNG" H 10050 1500 50  0001 C CNN "Manufacturer"
F 8 "C113206" H 10050 1500 50  0001 C CNN "lcsc-part"
	1    10050 1500
	1    0    0    -1  
$EndComp
Text GLabel 9000 1200 0    50   Input ~ 0
CS
Text GLabel 9000 1300 0    50   Input ~ 0
MOSI
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0137
U 1 1 5D636DF5
P 9000 1400
F 0 "#PWR0137" H 9000 1250 50  0001 C CNN
F 1 "+3.3V" V 9015 1528 50  0000 L CNN
F 2 "" H 9000 1400 50  0001 C CNN
F 3 "" H 9000 1400 50  0001 C CNN
	1    9000 1400
	0    -1   -1   0   
$EndComp
Text GLabel 9000 1500 0    50   Input ~ 0
SCK
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0138
U 1 1 5D63E2E4
P 9000 1600
F 0 "#PWR0138" H 9000 1350 50  0001 C CNN
F 1 "GND" V 9005 1472 50  0000 R CNN
F 2 "" H 9000 1600 50  0001 C CNN
F 3 "" H 9000 1600 50  0001 C CNN
	1    9000 1600
	0    1    1    0   
$EndComp
Text GLabel 9000 1700 0    50   Input ~ 0
MISO
Text GLabel 9000 1900 0    50   Input ~ 0
PA16
NoConn ~ 9350 1800
NoConn ~ 9350 1100
Wire Wire Line
	9000 1300 9350 1300
Wire Wire Line
	9000 1400 9200 1400
Wire Wire Line
	9000 1500 9350 1500
Wire Wire Line
	9000 1600 9350 1600
Wire Wire Line
	9000 1700 9350 1700
Wire Wire Line
	9350 1900 9000 1900
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0139
U 1 1 5D654E92
P 10350 2200
F 0 "#PWR0139" H 10350 1950 50  0001 C CNN
F 1 "GND" H 10355 2027 50  0000 C CNN
F 2 "" H 10350 2200 50  0001 C CNN
F 3 "" H 10350 2200 50  0001 C CNN
	1    10350 2200
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0140
U 1 1 5D654F44
P 10450 2200
F 0 "#PWR0140" H 10450 1950 50  0001 C CNN
F 1 "GND" H 10455 2027 50  0000 C CNN
F 2 "" H 10450 2200 50  0001 C CNN
F 3 "" H 10450 2200 50  0001 C CNN
	1    10450 2200
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0141
U 1 1 5D654F8B
P 10450 700
F 0 "#PWR0141" H 10450 450 50  0001 C CNN
F 1 "GND" H 10455 527 50  0000 C CNN
F 2 "" H 10450 700 50  0001 C CNN
F 3 "" H 10450 700 50  0001 C CNN
	1    10450 700 
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0142
U 1 1 5D65500E
P 10350 700
F 0 "#PWR0142" H 10350 450 50  0001 C CNN
F 1 "GND" H 10355 527 50  0000 C CNN
F 2 "" H 10350 700 50  0001 C CNN
F 3 "" H 10350 700 50  0001 C CNN
	1    10350 700 
	-1   0    0    1   
$EndComp
Wire Wire Line
	10350 700  10350 750 
Wire Wire Line
	10450 700  10450 750 
Wire Wire Line
	10450 2150 10450 2200
Wire Wire Line
	10350 2150 10350 2200
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0143
U 1 1 5D660369
P 9200 2300
F 0 "#PWR0143" H 9200 2050 50  0001 C CNN
F 1 "GND" H 9205 2127 50  0000 C CNN
F 2 "" H 9200 2300 50  0001 C CNN
F 3 "" H 9200 2300 50  0001 C CNN
	1    9200 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 2300 9200 2250
Wire Wire Line
	9200 1950 9200 1400
Connection ~ 9200 1400
Wire Wire Line
	9200 1400 9350 1400
Text GLabel 6800 2000 2    50   Input ~ 0
PA28
Text GLabel 850  2600 0    50   Input ~ 0
PA2
Text GLabel 850  2500 0    50   Input ~ 0
AREF
Text GLabel 2000 2300 2    50   Input ~ 0
PB8
Text GLabel 850  2900 0    50   Input ~ 0
PA4
Text GLabel 850  3000 0    50   Input ~ 0
PA5
Text GLabel 850  3100 0    50   Input ~ 0
PA6
Text GLabel 2000 3300 2    50   Input ~ 0
PA8
Text GLabel 2000 3200 2    50   Input ~ 0
PA9
Text GLabel 850  3500 0    50   Input ~ 0
PA10
Text GLabel 850  3800 0    50   Input ~ 0
PA11
Text GLabel 850  3700 0    50   Input ~ 0
PB10
Text GLabel 850  3600 0    50   Input ~ 0
PB11
Text GLabel 2000 3600 2    50   Input ~ 0
MOSI
Text GLabel 2000 3500 2    50   Input ~ 0
SCK
Text GLabel 2000 3400 2    50   Input ~ 0
MISO
Text GLabel 850  2800 0    50   Input ~ 0
PB3
Text GLabel 850  2700 0    50   Input ~ 0
PB2
Text GLabel 850  2400 0    50   Input ~ 0
PA28
Text GLabel 2000 2900 2    50   Input ~ 0
RESET
Text GLabel 2000 3100 2    50   Input ~ 0
PB23
Text GLabel 2000 3000 2    50   Input ~ 0
PB22
Text GLabel 850  3400 0    50   Input ~ 0
PA23
Text GLabel 850  3300 0    50   Input ~ 0
PA22
Text GLabel 2000 3700 2    50   Input ~ 0
PA21
Text GLabel 850  2300 0    50   Input ~ 0
PA19
Text GLabel 2000 2400 2    50   Input ~ 0
PA17
Text GLabel 2000 2500 2    50   Input ~ 0
PA16
Text GLabel 850  3200 0    50   Input ~ 0
PA7
Text GLabel 2000 3800 2    50   Input ~ 0
PA20
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0144
U 1 1 5D684B64
P 2000 2800
F 0 "#PWR0144" H 2000 2550 50  0001 C CNN
F 1 "GND" V 2005 2672 50  0000 R CNN
F 2 "" H 2000 2800 50  0001 C CNN
F 3 "" H 2000 2800 50  0001 C CNN
	1    2000 2800
	0    -1   -1   0   
$EndComp
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0145
U 1 1 5D684BA9
P 2000 2700
F 0 "#PWR0145" H 2000 2550 50  0001 C CNN
F 1 "+3.3V" V 2000 2950 50  0000 C CNN
F 2 "" H 2000 2700 50  0001 C CNN
F 3 "" H 2000 2700 50  0001 C CNN
	1    2000 2700
	0    1    1    0   
$EndComp
Text GLabel 2000 2600 2    50   Input ~ 0
V_USB
Text GLabel 3300 2000 0    50   Input ~ 0
PB9
Text GLabel 2000 2200 2    50   Input ~ 0
PB9
Text GLabel 850  2200 0    50   Input ~ 0
PA27
Text Notes 7400 7500 0    50   ~ 0
AArhus Mini Maker Faire 2019
Wire Wire Line
	5650 4300 5750 4300
Connection ~ 5750 4300
Wire Wire Line
	4300 5300 4400 5300
Connection ~ 4400 5300
Text Notes 7100 2350 0    50   ~ 0
Red LED
Text GLabel 1050 4450 1    50   Input ~ 0
PA22
Text GLabel 1050 5550 3    50   Input ~ 0
PA23
Text GLabel 1250 4450 1    50   Input ~ 0
PA20
Text GLabel 1250 5550 3    50   Input ~ 0
PA21
Text Notes 7050 3200 0    50   ~ 0
sao scl\nsao sda
Text GLabel 5650 4300 0    50   Input ~ 0
PA17
Text GLabel 4300 5300 0    50   Input ~ 0
PA2
Text Notes 7050 3750 0    50   ~ 0
sw3
Wire Wire Line
	9000 1200 9350 1200
Wire Wire Line
	4350 4300 4400 4300
Connection ~ 4400 4300
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J1
U 1 1 5D6343CF
P 1050 2200
F 0 "J1" H 1100 2200 50  0000 L CNN
F 1 "PA27" H 1200 2200 50  0000 L CNN
F 2 "misc:pad" H 1050 2200 50  0001 C CNN
F 3 "~" H 1050 2200 50  0001 C CNN
F 4 "np" H 1050 2200 50  0001 C CNN "NP"
	1    1050 2200
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J2
U 1 1 5D6348A5
P 1050 2300
F 0 "J2" H 1100 2300 50  0000 L CNN
F 1 "PA19" H 1200 2300 50  0000 L CNN
F 2 "misc:pad" H 1050 2300 50  0001 C CNN
F 3 "~" H 1050 2300 50  0001 C CNN
	1    1050 2300
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J5
U 1 1 5D6348EB
P 1050 2400
F 0 "J5" H 1100 2400 50  0000 L CNN
F 1 "PA28" H 1200 2400 50  0000 L CNN
F 2 "misc:pad" H 1050 2400 50  0001 C CNN
F 3 "~" H 1050 2400 50  0001 C CNN
	1    1050 2400
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J6
U 1 1 5D634937
P 1050 2500
F 0 "J6" H 1100 2500 50  0000 L CNN
F 1 "AREF" H 1200 2500 50  0000 L CNN
F 2 "misc:pad" H 1050 2500 50  0001 C CNN
F 3 "~" H 1050 2500 50  0001 C CNN
	1    1050 2500
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J7
U 1 1 5D634981
P 1050 2600
F 0 "J7" H 1100 2600 50  0000 L CNN
F 1 "PA2" H 1200 2600 50  0000 L CNN
F 2 "misc:pad" H 1050 2600 50  0001 C CNN
F 3 "~" H 1050 2600 50  0001 C CNN
	1    1050 2600
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J8
U 1 1 5D6349CD
P 1050 2700
F 0 "J8" H 1100 2700 50  0000 L CNN
F 1 "PB2" H 1200 2700 50  0000 L CNN
F 2 "misc:pad" H 1050 2700 50  0001 C CNN
F 3 "~" H 1050 2700 50  0001 C CNN
	1    1050 2700
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J9
U 1 1 5D634A1B
P 1050 2800
F 0 "J9" H 1100 2800 50  0000 L CNN
F 1 "PB3" H 1200 2800 50  0000 L CNN
F 2 "misc:pad" H 1050 2800 50  0001 C CNN
F 3 "~" H 1050 2800 50  0001 C CNN
	1    1050 2800
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J10
U 1 1 5D634A6B
P 1050 2900
F 0 "J10" H 1100 2900 50  0000 L CNN
F 1 "PA4" H 1200 2900 50  0000 L CNN
F 2 "misc:pad" H 1050 2900 50  0001 C CNN
F 3 "~" H 1050 2900 50  0001 C CNN
	1    1050 2900
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J11
U 1 1 5D634ABD
P 1050 3000
F 0 "J11" H 1100 3000 50  0000 L CNN
F 1 "PA5" H 1200 3000 50  0000 L CNN
F 2 "misc:pad" H 1050 3000 50  0001 C CNN
F 3 "~" H 1050 3000 50  0001 C CNN
	1    1050 3000
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J12
U 1 1 5D634B15
P 1050 3100
F 0 "J12" H 1100 3100 50  0000 L CNN
F 1 "PA6" H 1200 3100 50  0000 L CNN
F 2 "misc:pad" H 1050 3100 50  0001 C CNN
F 3 "~" H 1050 3100 50  0001 C CNN
	1    1050 3100
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J13
U 1 1 5D634B6B
P 1050 3200
F 0 "J13" H 1100 3200 50  0000 L CNN
F 1 "PA7" H 1200 3200 50  0000 L CNN
F 2 "misc:pad" H 1050 3200 50  0001 C CNN
F 3 "~" H 1050 3200 50  0001 C CNN
	1    1050 3200
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J14
U 1 1 5D634C0B
P 1050 3300
F 0 "J14" H 1100 3300 50  0000 L CNN
F 1 "PA22" H 1200 3300 50  0000 L CNN
F 2 "misc:pad" H 1050 3300 50  0001 C CNN
F 3 "~" H 1050 3300 50  0001 C CNN
	1    1050 3300
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J15
U 1 1 5D634C65
P 1050 3400
F 0 "J15" H 1100 3400 50  0000 L CNN
F 1 "PA23" H 1200 3400 50  0000 L CNN
F 2 "misc:pad" H 1050 3400 50  0001 C CNN
F 3 "~" H 1050 3400 50  0001 C CNN
	1    1050 3400
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J16
U 1 1 5D634CC1
P 1050 3500
F 0 "J16" H 1100 3500 50  0000 L CNN
F 1 "PA10" H 1200 3500 50  0000 L CNN
F 2 "misc:pad" H 1050 3500 50  0001 C CNN
F 3 "~" H 1050 3500 50  0001 C CNN
	1    1050 3500
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J17
U 1 1 5D634D1F
P 1050 3600
F 0 "J17" H 1100 3600 50  0000 L CNN
F 1 "PB11" H 1200 3600 50  0000 L CNN
F 2 "misc:pad" H 1050 3600 50  0001 C CNN
F 3 "~" H 1050 3600 50  0001 C CNN
	1    1050 3600
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J18
U 1 1 5D634D7F
P 1050 3700
F 0 "J18" H 1100 3700 50  0000 L CNN
F 1 "PB10" H 1200 3700 50  0000 L CNN
F 2 "misc:pad" H 1050 3700 50  0001 C CNN
F 3 "~" H 1050 3700 50  0001 C CNN
	1    1050 3700
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J19
U 1 1 5D634DE1
P 1050 3800
F 0 "J19" H 1100 3800 50  0000 L CNN
F 1 "PA11" H 1200 3800 50  0000 L CNN
F 2 "misc:pad" H 1050 3800 50  0001 C CNN
F 3 "~" H 1050 3800 50  0001 C CNN
	1    1050 3800
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J20
U 1 1 5D634EC3
P 1800 2200
F 0 "J20" H 1850 2200 50  0000 L CNN
F 1 "PB9" H 1950 2200 50  0000 L CNN
F 2 "misc:pad" H 1800 2200 50  0001 C CNN
F 3 "~" H 1800 2200 50  0001 C CNN
	1    1800 2200
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J21
U 1 1 5D634FA1
P 1800 2300
F 0 "J21" H 1850 2300 50  0000 L CNN
F 1 "PB8" H 1950 2300 50  0000 L CNN
F 2 "misc:pad" H 1800 2300 50  0001 C CNN
F 3 "~" H 1800 2300 50  0001 C CNN
	1    1800 2300
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J22
U 1 1 5D635009
P 1800 2400
F 0 "J22" H 1850 2400 50  0000 L CNN
F 1 "PA17" H 1950 2400 50  0000 L CNN
F 2 "misc:pad" H 1800 2400 50  0001 C CNN
F 3 "~" H 1800 2400 50  0001 C CNN
	1    1800 2400
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J23
U 1 1 5D635073
P 1800 2500
F 0 "J23" H 1850 2500 50  0000 L CNN
F 1 "PA16" H 1950 2500 50  0000 L CNN
F 2 "misc:pad" H 1800 2500 50  0001 C CNN
F 3 "~" H 1800 2500 50  0001 C CNN
	1    1800 2500
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J24
U 1 1 5D6350DF
P 1800 2600
F 0 "J24" H 1850 2600 50  0000 L CNN
F 1 "Vusb" H 1950 2600 50  0000 L CNN
F 2 "misc:pad" H 1800 2600 50  0001 C CNN
F 3 "~" H 1800 2600 50  0001 C CNN
	1    1800 2600
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J25
U 1 1 5D63514D
P 1800 2700
F 0 "J25" H 1850 2700 50  0000 L CNN
F 1 "3v3" H 1950 2700 50  0000 L CNN
F 2 "misc:pad" H 1800 2700 50  0001 C CNN
F 3 "~" H 1800 2700 50  0001 C CNN
	1    1800 2700
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J26
U 1 1 5D6351BD
P 1800 2800
F 0 "J26" H 1850 2800 50  0000 L CNN
F 1 "GND" H 1950 2800 50  0000 L CNN
F 2 "misc:pad" H 1800 2800 50  0001 C CNN
F 3 "~" H 1800 2800 50  0001 C CNN
	1    1800 2800
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J27
U 1 1 5D635355
P 1800 2900
F 0 "J27" H 1850 2900 50  0000 L CNN
F 1 "~RESET" H 1950 2900 50  0000 L CNN
F 2 "misc:pad" H 1800 2900 50  0001 C CNN
F 3 "~" H 1800 2900 50  0001 C CNN
	1    1800 2900
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J28
U 1 1 5D6353C9
P 1800 3000
F 0 "J28" H 1850 3000 50  0000 L CNN
F 1 "PB22" H 1950 3000 50  0000 L CNN
F 2 "misc:pad" H 1800 3000 50  0001 C CNN
F 3 "~" H 1800 3000 50  0001 C CNN
	1    1800 3000
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J29
U 1 1 5D63543F
P 1800 3100
F 0 "J29" H 1850 3100 50  0000 L CNN
F 1 "PB23" H 1950 3100 50  0000 L CNN
F 2 "misc:pad" H 1800 3100 50  0001 C CNN
F 3 "~" H 1800 3100 50  0001 C CNN
	1    1800 3100
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J30
U 1 1 5D6354B7
P 1800 3200
F 0 "J30" H 1850 3200 50  0000 L CNN
F 1 "PA9" H 1950 3200 50  0000 L CNN
F 2 "misc:pad" H 1800 3200 50  0001 C CNN
F 3 "~" H 1800 3200 50  0001 C CNN
	1    1800 3200
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J31
U 1 1 5D635531
P 1800 3300
F 0 "J31" H 1850 3300 50  0000 L CNN
F 1 "PA8" H 1950 3300 50  0000 L CNN
F 2 "misc:pad" H 1800 3300 50  0001 C CNN
F 3 "~" H 1800 3300 50  0001 C CNN
	1    1800 3300
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J32
U 1 1 5D63565D
P 1800 3400
F 0 "J32" H 1850 3400 50  0000 L CNN
F 1 "MISO" H 1950 3400 50  0000 L CNN
F 2 "misc:pad" H 1800 3400 50  0001 C CNN
F 3 "~" H 1800 3400 50  0001 C CNN
	1    1800 3400
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J33
U 1 1 5D6356DB
P 1800 3500
F 0 "J33" H 1850 3500 50  0000 L CNN
F 1 "SCK" H 1950 3500 50  0000 L CNN
F 2 "misc:pad" H 1800 3500 50  0001 C CNN
F 3 "~" H 1800 3500 50  0001 C CNN
	1    1800 3500
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J34
U 1 1 5D63575B
P 1800 3600
F 0 "J34" H 1850 3600 50  0000 L CNN
F 1 "MOSI" H 1950 3600 50  0000 L CNN
F 2 "misc:pad" H 1800 3600 50  0001 C CNN
F 3 "~" H 1800 3600 50  0001 C CNN
	1    1800 3600
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J35
U 1 1 5D635816
P 1800 3700
F 0 "J35" H 1850 3700 50  0000 L CNN
F 1 "PA21" H 1950 3700 50  0000 L CNN
F 2 "misc:pad" H 1800 3700 50  0001 C CNN
F 3 "~" H 1800 3700 50  0001 C CNN
	1    1800 3700
	-1   0    0    1   
$EndComp
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J36
U 1 1 5D63589A
P 1800 3800
F 0 "J36" H 1850 3800 50  0000 L CNN
F 1 "PA20" H 1950 3800 50  0000 L CNN
F 2 "misc:pad" H 1800 3800 50  0001 C CNN
F 3 "~" H 1800 3800 50  0001 C CNN
	1    1800 3800
	-1   0    0    1   
$EndComp
Text Notes 2950 1550 0    50   ~ 0
sw2
Text Notes 7050 3850 0    50   ~ 0
sd CD
Text Label 6800 1800 0    50   ~ 0
vddcore1
Text Notes 7050 3400 0    50   ~ 0
sao gpio1
Text Notes 7050 3300 0    50   ~ 0
sao gpio2
$Comp
L aammf-badge-2019-rescue:Conn_01x07_Female-Connector-aammf-badge-2019-rescue J37
U 1 1 5D63F97B
P 2050 4800
F 0 "J37" H 2077 4826 50  0000 L CNN
F 1 "st7789" H 2077 4735 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 2050 4800 50  0001 C CNN
F 3 "~" H 2050 4800 50  0001 C CNN
F 4 "np" H 2050 4800 50  0001 C CNN "NP"
	1    2050 4800
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0105
U 1 1 5D63FC6D
P 1750 4500
F 0 "#PWR0105" H 1750 4250 50  0001 C CNN
F 1 "GND" V 1750 4300 50  0000 C CNN
F 2 "" H 1750 4500 50  0001 C CNN
F 3 "" H 1750 4500 50  0001 C CNN
	1    1750 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 4500 1850 4500
$Comp
L aammf-badge-2019-rescue:+3.3V-power-aammf-badge-2019-rescue #PWR0127
U 1 1 5D6425DC
P 1750 4600
F 0 "#PWR0127" H 1750 4450 50  0001 C CNN
F 1 "+3.3V" V 1750 4850 50  0000 C CNN
F 2 "" H 1750 4600 50  0001 C CNN
F 3 "" H 1750 4600 50  0001 C CNN
	1    1750 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1750 4600 1850 4600
Text GLabel 1700 4700 0    50   Input ~ 0
PA4
Text GLabel 1700 4800 0    50   Input ~ 0
PA5
Text GLabel 1700 4900 0    50   Input ~ 0
PA6
Text GLabel 1700 5000 0    50   Input ~ 0
PA7
Text GLabel 1700 5100 0    50   Input ~ 0
PA8
Wire Wire Line
	1700 5100 1850 5100
Wire Wire Line
	1850 5000 1700 5000
Wire Wire Line
	1700 4900 1850 4900
Wire Wire Line
	1850 4800 1700 4800
Wire Wire Line
	1700 4700 1850 4700
Text Notes 2800 2400 0    50   ~ 0
lcd scl\nlcd sda\nlcd rst\nlcd dc
Text Notes 2800 2750 0    50   ~ 0
lcd bkl
$Sheet
S 5250 6600 700  800 
U 5D65EB24
F0 "charlixplex" 50
F1 "charlieplex-flat-matrix-7.sch" 50
F2 "L0" I L 5250 6700 50 
F3 "L1" I L 5250 6800 50 
F4 "L2" I L 5250 6900 50 
F5 "L3" I L 5250 7000 50 
F6 "L4" I L 5250 7100 50 
F7 "L5" I L 5250 7200 50 
F8 "L6" I L 5250 7300 50 
$EndSheet
Text GLabel 4750 6700 0    50   Input ~ 0
PB9
Text GLabel 4750 7100 0    50   Input ~ 0
PB10
Text GLabel 4750 7200 0    50   Input ~ 0
PB11
Text Notes 2650 2050 0    50   ~ 0
CharliePlex
Text Notes 7050 3550 0    50   ~ 0
Charlie 0
Wire Wire Line
	4750 6700 4850 6700
Wire Wire Line
	5150 6700 5250 6700
Wire Wire Line
	5250 6800 5150 6800
Wire Wire Line
	4850 6800 4750 6800
Wire Wire Line
	4750 6900 4850 6900
Wire Wire Line
	5150 6900 5250 6900
Wire Wire Line
	4750 7000 4850 7000
Wire Wire Line
	5150 7000 5250 7000
Wire Wire Line
	5250 7100 5150 7100
Wire Wire Line
	4850 7100 4750 7100
Wire Wire Line
	4750 7200 4850 7200
Wire Wire Line
	5150 7200 5250 7200
Wire Wire Line
	4750 7300 4850 7300
Wire Wire Line
	5150 7300 5250 7300
$Comp
L aammf-badge-2019-rescue:SMD-RES-49.9R-1%-1_16W_0402_-OPL_Resistor-aammf-badge-2019-rescue R2
U 1 1 5D688B86
P 5000 6700
F 0 "R2" H 4900 6750 45  0000 C CNN
F 1 "39 Ohm" H 4950 6850 45  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5000 6700 40  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/KOA-Speer-Elec-RK73H1ETTP39R0F_C211709.pdf" H 5000 6700 40  0001 C CNN
F 4 "RK73H1ETTP39R0F" H 5030 6850 20  0001 C CNN "MPN"
F 5 "301010041" H 5030 6850 20  0001 C CNN "seeed-SKU"
F 6 "KOA Speer Elec" H 5000 6700 50  0001 C CNN "Manufacturer"
F 7 "C211709" H 5000 6700 50  0001 C CNN "lcsc-part"
	1    5000 6700
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-RES-49.9R-1%-1_16W_0402_-OPL_Resistor-aammf-badge-2019-rescue R3
U 1 1 5D688DEE
P 5000 6800
F 0 "R3" H 4900 6850 45  0000 C CNN
F 1 "39 Ohm" H 4950 7050 45  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5000 6800 40  0001 C CNN
F 3 "" H 5000 6800 40  0001 C CNN
	1    5000 6800
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-RES-49.9R-1%-1_16W_0402_-OPL_Resistor-aammf-badge-2019-rescue R4
U 1 1 5D688E7E
P 5000 6900
F 0 "R4" H 4900 6950 45  0000 C CNN
F 1 "39 Ohm" H 4950 7250 45  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5000 6900 40  0001 C CNN
F 3 "" H 5000 6900 40  0001 C CNN
	1    5000 6900
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-RES-49.9R-1%-1_16W_0402_-OPL_Resistor-aammf-badge-2019-rescue R5
U 1 1 5D688F10
P 5000 7000
F 0 "R5" H 4900 7050 45  0000 C CNN
F 1 "39 Ohm" H 4950 7450 45  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5000 7000 40  0001 C CNN
F 3 "" H 5000 7000 40  0001 C CNN
	1    5000 7000
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-RES-49.9R-1%-1_16W_0402_-OPL_Resistor-aammf-badge-2019-rescue R6
U 1 1 5D688FA4
P 5000 7100
F 0 "R6" H 4900 7150 45  0000 C CNN
F 1 "39 Ohm" H 4950 7650 45  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5000 7100 40  0001 C CNN
F 3 "" H 5000 7100 40  0001 C CNN
	1    5000 7100
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-RES-49.9R-1%-1_16W_0402_-OPL_Resistor-aammf-badge-2019-rescue R7
U 1 1 5D68903C
P 5000 7200
F 0 "R7" H 4900 7250 45  0000 C CNN
F 1 "39 Ohm" H 4950 7850 45  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5000 7200 40  0001 C CNN
F 3 "" H 5000 7200 40  0001 C CNN
	1    5000 7200
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:SMD-RES-49.9R-1%-1_16W_0402_-OPL_Resistor-aammf-badge-2019-rescue R8
U 1 1 5D6890D4
P 5000 7300
F 0 "R8" H 4900 7350 45  0000 C CNN
F 1 "39 Ohm" H 4950 8050 45  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5000 7300 40  0001 C CNN
F 3 "" H 5000 7300 40  0001 C CNN
	1    5000 7300
	1    0    0    -1  
$EndComp
Text GLabel 850  2050 0    50   Input ~ 0
PA18
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J38
U 1 1 5D6C05E5
P 1050 2050
F 0 "J38" H 1100 2050 50  0000 L CNN
F 1 "PA18" H 1200 2050 50  0000 L CNN
F 2 "misc:pad" H 1050 2050 50  0001 C CNN
F 3 "~" H 1050 2050 50  0001 C CNN
	1    1050 2050
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:HRO-TYPE-C-31-M-12-Type-C-aammf-badge-2019-rescue USB1
U 1 1 5D6DE7F6
P 3500 6750
F 0 "USB1" H 3331 7547 60  0000 C CNN
F 1 "HRO-TYPE-C-31-M-12" H 3331 7441 60  0000 C CNN
F 2 "Type-C:HRO-TYPE-C-31-M-12-HandSoldering" H 3500 6750 60  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/1903211732_Korean-Hroparts-Elec-TYPE-C-31-M-12_C165948.pdf" H 3500 6750 60  0001 C CNN
F 4 "Korean Hroparts Elec" H 3500 6750 50  0001 C CNN "Manufacturer"
F 5 "TYPE-C-31-M-12" H 3500 6750 50  0001 C CNN "MPN"
F 6 "C165948" H 3500 6750 50  0001 C CNN "lcsc-part"
	1    3500 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 6300 3750 6300
Text GLabel 3750 7200 2    50   Input ~ 0
V_USB
Wire Wire Line
	3750 7200 3600 7200
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0132
U 1 1 5D6EBEB5
P 3750 7400
F 0 "#PWR0132" H 3750 7150 50  0001 C CNN
F 1 "GND" V 3755 7272 50  0000 R CNN
F 2 "" H 3750 7400 50  0001 C CNN
F 3 "" H 3750 7400 50  0001 C CNN
	1    3750 7400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 7400 3600 7400
Wire Wire Line
	3600 7300 3750 7300
Wire Wire Line
	3750 7300 3750 7400
Connection ~ 3750 7400
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0134
U 1 1 5D6F49AD
P 3750 6200
F 0 "#PWR0134" H 3750 5950 50  0001 C CNN
F 1 "GND" V 3755 6072 50  0000 R CNN
F 2 "" H 3750 6200 50  0001 C CNN
F 3 "" H 3750 6200 50  0001 C CNN
	1    3750 6200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 6200 3600 6200
Wire Wire Line
	3750 6700 3700 6700
Wire Wire Line
	3600 6600 3650 6600
Wire Wire Line
	3600 6800 3650 6800
Wire Wire Line
	3650 6800 3650 6600
Connection ~ 3650 6600
Wire Wire Line
	3650 6600 3750 6600
Wire Wire Line
	3700 6700 3700 6900
Wire Wire Line
	3700 6900 3600 6900
Connection ~ 3700 6700
Wire Wire Line
	3700 6700 3600 6700
NoConn ~ 3600 6400
NoConn ~ 3600 6500
NoConn ~ 3600 7000
NoConn ~ 3600 7100
Connection ~ 10650 4100
$Comp
L aammf-badge-2019-rescue:BL8536-bl8536-aammf-badge-2019-rescue IC2
U 1 1 5D693576
P 9650 5300
F 0 "IC2" H 9650 5350 50  0000 C CNN
F 1 "BL8536CB3TR33 3.3V boost" H 9650 5474 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9650 5200 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Shanghai-Belling-BL8536CB3TR_C97793.pdf" H 9400 4600 50  0001 C CNN
F 4 "Shanghai Belling" H 9600 5650 50  0001 C CNN "Manufacturer"
F 5 "BL8536CB3TR33" H 9600 5550 50  0001 C CNN "MPN"
F 6 "C97793" H 9650 5300 50  0001 C CNN "lcsc-part"
	1    9650 5300
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0103
U 1 1 5D6AD367
P 9650 5850
F 0 "#PWR0103" H 9650 5600 50  0001 C CNN
F 1 "GND" H 9655 5677 50  0000 C CNN
F 2 "" H 9650 5850 50  0001 C CNN
F 3 "" H 9650 5850 50  0001 C CNN
	1    9650 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 5850 9650 5800
$Comp
L aammf-badge-2019-rescue:C-Device-aammf-badge-2019-rescue C9
U 1 1 5D6B7A81
P 8450 5550
F 0 "C9" H 8565 5596 50  0000 L CNN
F 1 "10 uF" H 8565 5505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8488 5400 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/1812111235_Murata-Electronics-GRM188Z71A106MA73D_C237435.pdf" H 8450 5550 50  0001 C CNN
F 4 "Murata Electronics" H 8450 5550 50  0001 C CNN "Manufacturer"
F 5 "GRM188Z71A106MA73D" H 8450 5550 50  0001 C CNN "MPN"
F 6 "C237435" H 8450 5550 50  0001 C CNN "lcsc-part"
	1    8450 5550
	1    0    0    -1  
$EndComp
$Comp
L aammf-badge-2019-rescue:INDUCTOR-pspice-aammf-badge-2019-rescue L1
U 1 1 5D6B7C51
P 8800 5300
F 0 "L1" H 8800 5515 50  0000 C CNN
F 1 "4.7 uH" H 8800 5424 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 8800 5300 50  0001 C CNN
F 3 "http://file.elecfans.com/web1/M00/8D/C8/pIYBAFylVpyAek3vAAPPxerVryw992.pdf?filename=MLP2012_TDK.pdf" H 8800 5300 50  0001 C CNN
F 4 "TDK" H 8800 5300 50  0001 C CNN "Manufacturer"
F 5 "MLP2012S4R7MT0S1" H 8800 5300 50  0001 C CNN "MPN"
F 6 "C87543" H 8800 5300 50  0001 C CNN "lcsc-part"
	1    8800 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 5300 8450 5300
Wire Wire Line
	8450 5400 8450 5300
Connection ~ 8450 5300
Wire Wire Line
	8450 5300 8550 5300
Wire Wire Line
	10250 5300 10150 5300
Wire Wire Line
	9150 5300 9050 5300
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0121
U 1 1 5D6EC7A5
P 8450 5850
F 0 "#PWR0121" H 8450 5600 50  0001 C CNN
F 1 "GND" H 8455 5677 50  0000 C CNN
F 2 "" H 8450 5850 50  0001 C CNN
F 3 "" H 8450 5850 50  0001 C CNN
	1    8450 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 5850 8450 5700
$Comp
L aammf-badge-2019-rescue:C-Device-aammf-badge-2019-rescue C10
U 1 1 5D6F1FC6
P 10250 5500
F 0 "C10" H 10365 5546 50  0000 L CNN
F 1 "10 uF" H 10365 5455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10288 5350 50  0001 C CNN
F 3 "~" H 10250 5500 50  0001 C CNN
	1    10250 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 5300 10250 5350
Connection ~ 10250 5300
$Comp
L aammf-badge-2019-rescue:GND-power-aammf-badge-2019-rescue #PWR0122
U 1 1 5D6F774B
P 10250 5850
F 0 "#PWR0122" H 10250 5600 50  0001 C CNN
F 1 "GND" H 10255 5677 50  0000 C CNN
F 2 "" H 10250 5850 50  0001 C CNN
F 3 "" H 10250 5850 50  0001 C CNN
	1    10250 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 5650 10250 5850
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J39
U 1 1 5D70D00F
P 10450 5300
F 0 "J39" H 10500 5300 50  0000 L CNN
F 1 "VBoost" H 10700 5300 50  0000 L CNN
F 2 "misc:pad" H 10450 5300 50  0001 C CNN
F 3 "~" H 10450 5300 50  0001 C CNN
	1    10450 5300
	1    0    0    -1  
$EndComp
Text GLabel 8050 5300 0    50   Input ~ 0
Vbat
Wire Wire Line
	8050 5300 8200 5300
Connection ~ 8200 5300
$Comp
L aammf-badge-2019-rescue:SMD-RES-1.5K-5%-1_10W_0603_-OPL_Resistor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue R9
U 1 1 5D71EAC7
P 8200 4950
F 0 "R9" V 8158 4998 45  0000 L CNN
F 1 "100k" V 8242 4998 45  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8200 4950 40  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-RC0603FR-07100KL_C14675.pdf" H 8200 4950 40  0001 C CNN
F 4 "RC0603FR-07100KL" H 8230 5100 20  0001 C CNN "MPN"
F 5 "301010114" H 8230 5100 20  0001 C CNN "seeed-SKU"
F 6 "YAGEO" V 8200 4950 50  0001 C CNN "Manufacturer"
F 7 "C14675" V 8200 4950 50  0001 C CNN "lcsc-part"
	1    8200 4950
	0    1    1    0   
$EndComp
Text GLabel 8050 4800 0    50   Input ~ 0
PA2
$Comp
L aammf-badge-2019-rescue:Conn_01x01_Female-Connector-aammf-badge-2019-rescue J40
U 1 1 5D72B40B
P 7950 5150
F 0 "J40" H 8000 5150 50  0000 L CNN
F 1 "Vbat" H 7950 5050 50  0000 L CNN
F 2 "misc:pad" H 7950 5150 50  0001 C CNN
F 3 "~" H 7950 5150 50  0001 C CNN
	1    7950 5150
	-1   0    0    1   
$EndComp
Wire Wire Line
	8200 5100 8200 5150
Wire Wire Line
	8050 4800 8200 4800
Wire Wire Line
	8150 5150 8200 5150
Connection ~ 8200 5150
Wire Wire Line
	8200 5150 8200 5300
Text Label 9350 3550 0    50   ~ 0
Vldo
Text Label 10250 5200 0    50   ~ 0
Vboost
Text Label 9050 5300 0    50   ~ 0
Vsw
Text Label 2750 1300 0    50   ~ 0
Xin32
Text Label 2750 1400 0    50   ~ 0
Xout32
Text Label 2450 4750 0    50   ~ 0
ledr
$Comp
L power:GND #PWR0146
U 1 1 5D8205E4
P 3700 4350
F 0 "#PWR0146" H 3700 4100 50  0001 C CNN
F 1 "GND" H 3705 4177 50  0000 C CNN
F 2 "" H 3700 4350 50  0001 C CNN
F 3 "" H 3700 4350 50  0001 C CNN
	1    3700 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4350 3700 4300
Text Notes 8150 7650 0    50   ~ 0
2019-09-07
$Comp
L OPL_Discrete_Semiconductor:SMD-MOSFET-N-CH-20V-2.1A-CJ2302_SOT-23_ Q1
U 1 1 5D837701
P 10250 3800
F 0 "Q1" H 10365 3842 45  0000 L CNN
F 1 "CJ2302" H 10300 3950 45  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 10250 3800 50  0001 C CNN
F 3 "https://statics3.seeedstudio.com/images/opl/datasheet/0440200P1.pdf" H 10250 3800 50  0001 C CNN
F 4 "CJ2302" H 10280 3950 20  0001 C CNN "MPN"
F 5 "305030015" H 10280 3950 20  0001 C CNN "seeed-SKU"
F 6 "SMD-MOSFET-N-CH-20V-2.1A-CJ2302_SOT-23_" H 10250 3800 50  0001 C CNN "Description"
F 7 "JCET" H 10250 3800 50  0001 C CNN "Manufacturer"
	1    10250 3800
	1    0    0    -1  
$EndComp
$Comp
L OPL_Discrete_Semiconductor:SMD-MOSFET-P-CH-8V-4.1A-CJ2305_SOT-23_ Q2
U 1 1 5D839D0F
P 10250 4400
F 0 "Q2" H 10364 4442 45  0000 L CNN
F 1 "CJ2305" H 10300 4550 45  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 10250 4400 50  0001 C CNN
F 3 "https://statics3.seeedstudio.com/images/opl/datasheet/0440210P1.pdf" H 10250 4400 50  0001 C CNN
F 4 "CJ2305" H 10280 4550 20  0001 C CNN "MPN"
F 5 "305030014" H 10280 4550 20  0001 C CNN "seeed-SKU"
F 6 "SMD-MOSFET-P-CH-8V-4.1A-CJ2305_SOT-23_" H 10250 4400 50  0001 C CNN "Description"
F 7 "JCET" H 10250 4400 50  0001 C CNN "Manufacturer"
	1    10250 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 4600 10250 5300
Wire Wire Line
	10250 4200 10250 4100
Wire Wire Line
	10250 4100 10650 4100
Connection ~ 10250 4100
Wire Wire Line
	10250 4100 10250 4000
Wire Wire Line
	10250 3550 10250 3600
Wire Wire Line
	9350 3550 10250 3550
Wire Wire Line
	8200 3550 8200 3150
Wire Wire Line
	8200 3150 9800 3150
Wire Wire Line
	9800 3150 9800 3800
Wire Wire Line
	10050 3800 9800 3800
Connection ~ 9800 3800
Wire Wire Line
	9800 4400 10050 4400
Connection ~ 9800 4400
Wire Wire Line
	9800 4400 9800 4500
$Comp
L power:GND #PWR0147
U 1 1 5D89A3D0
P 9800 4850
F 0 "#PWR0147" H 9800 4600 50  0001 C CNN
F 1 "GND" H 9805 4677 50  0000 C CNN
F 2 "" H 9800 4850 50  0001 C CNN
F 3 "" H 9800 4850 50  0001 C CNN
	1    9800 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 4850 9800 4800
Text GLabel 4750 6800 0    50   Input ~ 0
PB2
Text GLabel 4750 7300 0    50   Input ~ 0
PB3
Text GLabel 2450 4350 1    50   Input ~ 0
PB23
Text GLabel 4750 6900 0    50   Input ~ 0
PB22
Text GLabel 4750 7000 0    50   Input ~ 0
PB8
Text Notes 2650 1950 0    50   ~ 0
CharliePlex
Text Notes 2600 3350 0    50   ~ 0
CharliePlex
Text Notes 2600 3450 0    50   ~ 0
CharliePlex
Text Notes 7100 2450 0    50   ~ 0
CharliePlex
Text Notes 7100 1450 0    50   ~ 0
CharliePlex
Text Notes 7100 1350 0    50   ~ 0
CharliePlex
Wire Wire Line
	9800 3800 9800 4400
$Comp
L aammf-badge-2019-rescue:SMD-RES-1.5K-5%-1_10W_0603_-OPL_Resistor-samd21-dev-rescue-aammf-badge-2019-rescue-aammf-badge-2019-rescue R10
U 1 1 5D84E2E1
P 9800 4650
F 0 "R10" V 9758 4698 45  0000 L CNN
F 1 "100k" V 9842 4698 45  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9800 4650 40  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-RC0603FR-07100KL_C14675.pdf" H 9800 4650 40  0001 C CNN
F 4 "RC0603FR-07100KL" H 9830 4800 20  0001 C CNN "MPN"
F 5 "301010114" H 9830 4800 20  0001 C CNN "seeed-SKU"
F 6 "YAGEO" V 9800 4650 50  0001 C CNN "Manufacturer"
F 7 "C14675" V 9800 4650 50  0001 C CNN "lcsc-part"
	1    9800 4650
	0    1    1    0   
$EndComp
$EndSCHEMATC
