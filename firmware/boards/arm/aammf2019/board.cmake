# Copyright (c) 2019 Flemming Frandsen <dren.dk@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

include(${ZEPHYR_BASE}/boards/common/openocd.board.cmake)
