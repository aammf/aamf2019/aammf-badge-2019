#include "charlieplex.h"
#include "thread.h"

// Setting this turns setting a single charlieplex LED from a 61 us operation into a 4.8 us one.
#define FAST_GPIO 


#ifdef FAST_GPIO

#include "soc.h"

#else
static struct device* ports[2];
#endif


typedef struct {
  uint8_t port;
  uint8_t pin;
} CharliePlexPin;

// TODO: How to get this from DT?
CharliePlexPin charliePlexPins[7] = {
     {0, 9},
     {0, 10},
     {0, 11},
     {0, 19},
     {1, 9},
     {1, 10},
     {1, 11}		     
    };

// Set a pin drive high or low
void cxPin(CharliePlexPin pin, bool anode) {
#ifdef FAST_GPIO
  // This is *much* faster than the zephyr API:
  uint32_t mask = 1<<pin.pin;
  if (pin.port == 0) {
    if (anode) {
      REG_PORT_OUTSET0 = mask;
    } else {
      REG_PORT_OUTCLR0 = mask;
    }
    REG_PORT_DIRSET0 = mask;
  } else {
    if (anode) {
      REG_PORT_OUTSET1 = mask;
    } else {
      REG_PORT_OUTCLR1 = mask;
    }
    REG_PORT_DIRSET1 = mask;
  }
#else
  gpio_pin_configure(ports[pin.port], pin.pin, GPIO_DIR_OUT);
  gpio_pin_write(ports[pin.port], pin.pin, anode);
#endif
}

// Set all pins to high-z
void cxOff() {
#ifdef FAST_GPIO  
  // This is 23 us faster than the loop+gpio_pin_configure approach:
  const uint32_t PORTA_MASK = (1<<9) | (1<<10) | (1<<11) | (1<<19);
  const uint32_t PORTB_MASK = (1<<9) | (1<<10) | (1<<11);
  REG_PORT_DIRCLR0 = PORTA_MASK;
  REG_PORT_OUTCLR0 = PORTA_MASK;
  REG_PORT_DIRCLR1 = PORTB_MASK;
  REG_PORT_OUTCLR1 = PORTB_MASK;
#else  
  for (uint8_t i=0;i<sizeof(charliePlexPins)/sizeof(CharliePlexPin);i++) { 
    CharliePlexPin pin = charliePlexPins[i];
    gpio_pin_configure(ports[pin.port], pin.pin, GPIO_DIR_IN);
  }
#endif  
}

typedef struct {
  uint8_t anode;
  uint8_t cathode;
} CharliePlexLed;

// This is the actual charlieplex matrix
CharliePlexLed charliePlexLed[] =
  {
   {0, 1}, {1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0},
   {0, 2}, {1, 2}, {2, 1}, {3, 1}, {4, 1}, {5, 1}, {6, 1},
   {0, 3}, {1, 3}, {2, 3}, {3, 2}, {4, 2}, {5, 2}, {6, 2},
   {0, 4}, {1, 4}, {2, 4}, {3, 4}, {4, 3}, {5, 3}, {6, 3},
   {0, 5}, {1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 4}, {6, 4},
   {0, 6}, {1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 5}
  };

// Turn on a single LED
void cxLed(uint8_t ledIndex) {
  cxOff();
  if (ledIndex < sizeof(charliePlexLed)/sizeof(CharliePlexLed)) {
    CharliePlexLed led = charliePlexLed[ledIndex];
    cxPin(charliePlexPins[led.anode], true);
    cxPin(charliePlexPins[led.cathode], false);
  }
}

uint64_t cxLEDS;
// Sets the LEDs to turn on
void cxSetLeds(uint64_t newCxLEDs) {
  cxLEDS = newCxLEDs;
}

/**
 * This thread will loop rather tightly and light the charlie plex LEDs one at a time
 * To make this stable
 */

THREAD(cx, 500) {
  while (1) {
    uint64_t cx = cxLEDS;   
    for (uint8_t i=0;i<42;i++) {
      if (cx & 1) {
	//gpio_pin_write(ports[0], 22, 1);
	cxLed(i);
	//gpio_pin_write(ports[0], 22, 0);
	k_yield(); 
      }

      cx = cx >> 1;
    }
  }
}

void cxInit(void) {
#ifndef FAST_GPIO
  // TODO: How to get this from DT?
  ports[0] = device_get_binding(DT_ATMEL_SAM0_GPIO_PORT_A_LABEL);
  ports[1] = device_get_binding(DT_ATMEL_SAM0_GPIO_PORT_B_LABEL);
#endif
  
  START_THREAD(cx, 10); 
}
