#pragma once

#include <zephyr.h>

#define THREAD(name, stack)\
  K_THREAD_STACK_DEFINE(name##ThreadStack, stack);\
  struct k_thread name##ThreadData;\
  void name##Thread(void *a, void *b, void *c) 

#define START_THREAD(name, priority)\
  k_thread_create(&name##ThreadData, name##ThreadStack,	\
    K_THREAD_STACK_SIZEOF(name##ThreadStack),\
    name##Thread, NULL, NULL, NULL, priority, 0, K_NO_WAIT)
