/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <sys/printk.h>
#include <device.h>
#include <drivers/gpio.h>
#include <drivers/pinmux.h>
#include <random/rand32.h> 
#include "charlieplex.h"

#define LED_PORT DT_ALIAS_LED0_GPIOS_CONTROLLER
#define LED	 DT_ALIAS_LED0_GPIOS_PIN


void main(void) {
  cxInit();
  
  printk("Hello World! %s\n", CONFIG_BOARD);

  int cnt = 0;

  struct device *dev = device_get_binding(LED_PORT);
  /* Set LED pin as output */
  gpio_pin_configure(dev, LED, GPIO_DIR_OUT);
  gpio_pin_write(dev, LED, 1);

  int min = 0;
  int max = 0;
  int dir = 1;
  while (1) {

    uint64_t ncx = 0;
    for (uint8_t i=0;i<=42;i++) {
      ncx = ncx << 1;
      if (i >= min && i <= max) {
	if ((sys_rand32_get() & 0xff) >= 0xf0) {
	  ncx |= 1;
	}
      }
    }
    cxSetLeds(ncx);

    if (cnt++ > 1) {
      cnt = 0;

      min += dir;
      
      if (min == 0) {
	min = 0;
	dir = 1;
	gpio_pin_write(dev, LED, 0);
      }
      if (max >= 42) {
	dir = -1;
	gpio_pin_write(dev, LED, 1);
      }
    
      max = min+15;
    }

    k_sleep(30);
  }
}
