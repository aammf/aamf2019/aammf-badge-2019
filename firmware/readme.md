mkdir build
cd build

cmake -DBOARD=aammf2019 -DBOARD_ROOT=`pwd`/.. ..

make
make flash
