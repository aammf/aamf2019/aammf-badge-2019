# Aarhus Mini Maker Faire 2019 Badge

![Front view](gfx/front-09-07.png)
![Back view](gfx/back-09-07.png)

[Schematic in pdf](aammf-2019-09-07-schematic.pdf)

# Revisions
* 2019-08-27: First complete prototype
* 2019-08-28: Added boost converter to the battery to ensure that 3.3V is always available.
* 2019-08-29: Ordered JLCPCB green PCB prototype, stencil and parts for prototype.
    * Tried out bare board and copper for the bottom branding texts.
    * Made holes in the silkscreen for the front pads.
* 2019-09-02:
    * Removed part references from silkscreen.
    * Changed the bare PCB parts of the logos to silkscreen.
    * Changed nametag bubble.
    * Added PCBway Logo.
    * Changed the swd header footprint to 2.54mm pitch rather than 2mm
    * Some minor tweaking and cleaning up around the place.
* 2019-09-04:
    * Rotated some 0402 LEDs that were the wrong way around
    * Switched to a beefier coil in the boost converter
    * Added heat spreading zones for the LDO and the boost converter
* 2019-09-05:
    * Bumped the boost converter inductor to 4.7 uH to improve low-battery voltage performance.
    * Added a sense resistor to allow the MCU to measure the battery voltage.
    * Groomed the BOM fields so all parts have all fields filled.
    * Various minor tweaks.
    * Taped out for production.
* 2019-09-07:
    * Ensured that all components have nice 3d models    
    * Various minor layout improvements
    * Added a center hole for single-point hanging
    * Taped out for production, I hope.
* 2019-09-16
    * The mechanical switch has been a couple of FETs so the MCU is always powered,
    but preferentially from USB.
    * The charlieplexing pins all live on port b, along with the red LED.
    * The red LED should sit on PB22 or PB23 and be placed above the MCU.
    * Removed 2019 and pcbway branding as this version is for 2020
* 2019-09-19:
    * Taped out 2020 revision for PCBA at Seeed
* 2019-09-23:
    * Fixed L4 disconnect.


# Power supply performance

The goal is to be able to deliver at least 100 mA at 3.3 V to the board, regardless of
the power source.

## Boost converter

The boost converter is especially challenged as the battery voltage dropped, the prototype
converter can supply 100 mA with a cell-voltage of 1 V.

At 0.9 V of cell voltage (completely dead battery) the boost converter can deliver 90 mA
before sagging under 3.25 V, but at only 37% efficiency.

Completely full batteries with a cell voltage of 1.5 V allow a current draw of 200 mA
at 47% efficiency.

Bottom line is: Do not expect to pull more than 100 mA and if you do, expect your batteries
to last less than a day on full power.

The boost converter and the battery sense resistor are always connected to the batteries,
regardless of the power switch position, so together they will drain the batteries
in 3 years or so.


## LDO regulator

Power from the USB is a different matter as it's regulated by a tiny LDO with very little
cooling, but on the other hand the energy available is limitless.

The LDO will happily supply 150 mA forever, but it does get a bit hot.
than that will cause the voltage to sag.

The LDO will let the smoke out a some point, but 300 mA is possible for short bursts,
if you're ok with the voltage sagging to 3.1 V.


# Pinout (2019-09-07 version)

| Pin | Use |
|-----|-----|
| PA0 | 32KHz Crystal XIN32 |
| PA1 | 32KHz Crystal XOUT32 |
| PA2 | Switch 2 / V / pullup to Vbat |
| PA3 | Free / Aref |
| PA4 | LCD scl |
| PA5 | LCD sda |
| PA6 | LCD rst |
| PA7 | LCD dc |
| PA8 | LCD bkl |
| PA9 | Charlie 0 |
| PA10 | Charlie 1 |
| PA11 | Charlie 2 |
| PA12 | MOSI SD (scom2 pad0) |
| PA13 | SCK SD  (scom2 pad1)|
| PA14 | CS SD |
| PA15 | MISO SD (scom2 pad3)|
| PA16 | CD SD |
| PA17 | Switch 2 / H |
| PA18 | Free |
| PA19 | Charlie 3 |
| PA20 | SAO gpio 1 |
| PA21 | SAO gpio 2 |
| PA22 | SAO sda (scom5 pad0) |
| PA23 | SAO scl (scom5 pad1) |
| PA24 | USB - |
| PA25 | USB + |
| PA27 | Free |
| PA28 | Free |
| PA30 | SWD clk |
| PA31 | SWD io |
| PB2 | Free |
| PB3 | Free |
| PB8 | Red LED |
| PB9 | Charlie 4 |
| PB10 | Charlie 5 |
| PB11 | Charlie 6 |
| PB22 | Free |
| PB23 | Free |


# Pinout (2020 version)

| Pin | Use |
|-----|-----|
| PA0 | 32KHz Crystal XIN32 |
| PA1 | 32KHz Crystal XOUT32 |
| PA2 | Switch 2 / pullup to Vbat |
| PA3 | Free / Aref |
| PA4 | LCD scl |
| PA5 | LCD sda |
| PA6 | LCD rst |
| PA7 | LCD dc |
| PA8 | LCD bkl |
| PA9 | Free |
| PA10 | Free |
| PA11 | Free |
| PA12 | MOSI SD (scom2 pad0) |
| PA13 | SCK SD  (scom2 pad1) |
| PA14 | CS SD |
| PA15 | MISO SD (scom2 pad3)|
| PA16 | CD SD |
| PA17 | Switch 2 / H |
| PA18 | Free |
| PA19 | Free |
| PA20 | SAO gpio 1 |
| PA21 | SAO gpio 2 |
| PA22 | SAO sda (scom5 pad0) |
| PA23 | SAO scl (scom5 pad1) |
| PA24 | USB - |
| PA25 | USB + |
| PA27 | Free |
| PA28 | Free |
| PA30 | SWD clk |
| PA31 | SWD io |
| PB2 | Charlie 1 |
| PB3 | Charlie 6 |
| PB8 | Charlie 3 |
| PB9 | Charlie 0 |
| PB10 | Charlie 4 |
| PB11 | Charlie 5 |
| PB22 | Charlie 2 |
| PB23 | Red LED |


